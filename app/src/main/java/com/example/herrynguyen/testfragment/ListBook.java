package com.example.herrynguyen.testfragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class ListBook extends Fragment implements View.OnClickListener {
    private View rootView;
    private MainActivity main;
    private Button addBook, listBook;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        main = (MainActivity) getActivity();
        rootView = inflater.inflate(R.layout.addbook, null);
        addBook = (Button) rootView.findViewById(R.id.btnAddBook);
        listBook = (Button) rootView.findViewById(R.id.btnListBook);
        addBook.setOnClickListener(this);
        listBook.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddBook:
                Intent bookIntent1 = new Intent(getActivity().getBaseContext(), BookInfomation.class);
                bookIntent1.putExtra("tab", 0);
                getActivity().startActivity(bookIntent1);
                break;
            case R.id.btnListBook:
                Intent bookIntent2 = new Intent(getActivity().getBaseContext(), BookInfomation.class);
                bookIntent2.putExtra("tab", 1);
                getActivity().startActivity(bookIntent2);
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }
}
