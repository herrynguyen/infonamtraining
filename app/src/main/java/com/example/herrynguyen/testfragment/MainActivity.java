package com.example.herrynguyen.testfragment;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar.Tab;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.app.ProgressDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity {
    private ViewPager viewPager;
    private Fragment[] fragments;
    private MyPagerAdapter adapter;
    private ActionBar actionBar;
    private ActionBar.TabListener tabListener;
    private Intent intent;
    private ProgressDialog progressDialog;
    private AsyncTask<Void, Void, Void> waitServiceTask;
    public ClipboardManager clipboardManager;
    public Resources res;
    public InputMethodManager imm;
    private ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;
    private Uri targetUrl;
    public static DataAdapter data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            res = getResources();
            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            start();
        }
        data = new DataAdapter(getApplicationContext());
        data.createDB();
    }

    public void start() {
        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(3);
        fragments = new Fragment[3];
        fragments[0] = new ListBook();
        fragments[1] = new BookManage();
        fragments[2] = new BookManage();
        adapter = new MyPagerAdapter(getSupportFragmentManager(),
                fragments);
        viewPager.setAdapter(adapter);
        viewPager
                .setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                    @Override
                    public void onPageSelected(int pos) {
                        actionBar.setSelectedNavigationItem(pos);
                    }

                    @Override
                    public void onPageScrolled(int arg0,
                                               float arg1, int arg2) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void onPageScrollStateChanged(int arg0) {
                        // TODO Auto-generated method stub
                    }
                });
        tabListener = new ActionBar.TabListener() {

            @Override
            public void onTabUnselected(Tab tab,
                                        FragmentTransaction ft) {

            }

            @Override
            public void onTabSelected(Tab tab,
                                      FragmentTransaction ft) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabReselected(Tab tab,
                                        FragmentTransaction ft) {

            }
        };
        actionBar.addTab(actionBar.newTab()
                        .setText(res.getString(R.string.first))
                        .setTabListener(tabListener)
                        .setIcon(R.drawable.search)

        );
        actionBar.addTab(actionBar.newTab()
                        .setText(res.getString(R.string.second))
                        .setTabListener(tabListener)
                        .setIcon(R.drawable.search)

        );
        actionBar.addTab(actionBar.newTab()
                        .setText(res.getString(R.string.second))
                        .setTabListener(tabListener)
                        .setIcon(R.drawable.search)
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
