package com.example.herrynguyen.testfragment;


import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.Settings;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DataAdapter {
    public Activity activity;
    private static final String DATABASE_NAME = "BookLibraries";
    private static final int DATABASE_VERSION = 11;
    private static final String TAG = "DataAdapter";
    private final Context context;
    public static DatabaseHelpert DBHelper;
    private SQLiteDatabase db;

    public DataAdapter(Context ctx) {
        this.context = ctx;
    }

    public void createDB() {
        DBHelper = new DatabaseHelpert(context);
        Log.e("createDB", "createDB");
        Log.i("createDB", "Book info:" + BOOKINFO);
        open();
    }

    /*-------------------------Book------------------------------*/
    public static final String columBookID = "_id";
    public static final String bookName = "bookName";
    public static final String auThor = "author";
    public static final String publisherCom = "pc";
    private static final String BookInfomation = "BookInfomation";

    private static final String BOOKINFO = "CREATE TABLE "
            + BookInfomation + "(" + columBookID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + bookName + " TEXT, "
            + auThor + " TEXT, "
            + publisherCom + " TEXT)";
    /*-------------------------Borrow Book------------------------------*/
    public static final String columBorrowID = "_id";
    public static final String borrowDate = "borrowDate";
    public static final String borrowbookName = "borrowBookName";
    public static final String readerName = "readerName";
    private static final String BorrowBookTable = "BorrowBook";

    private static final String BORROWBOOK = "CREATE TABLE "
            + BorrowBookTable + "(" + columBorrowID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + borrowDate + " DATE, "
            + borrowbookName + " TEXT, "
            + readerName + " TEXT)";
    /*-------------------------------------------------------------------------*/

    private static class DatabaseHelpert extends SQLiteOpenHelper {
        DatabaseHelpert(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            Log.e("DatabaseHelpert", "getDatabaseName:" + getDatabaseName());
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // TODO Auto-generated method stub
            try {
                Log.e("onCreate", "Create Table:" + BOOKINFO);
                db.execSQL(BOOKINFO);
                db.execSQL(BORROWBOOK);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub
            Log.w(TAG, oldVersion + " to " + newVersion
                    + ", which will destroy all old data");
            //db.execSQL("DROP TABLE IF EXISTS BookInfomation");
            // onCreate(db);
        }
    }

    public DataAdapter open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        DBHelper.close();
    }

    /*-------------------------insertBook----------------------------*/
    public long insertBook(String name, String authors, String pcs) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(bookName, name);
        initialValues.put(auThor, authors);
        initialValues.put(publisherCom, pcs);
        open();
        return db.insert(BookInfomation, null, initialValues);
    }

    /*----------------------------all book----------------------*/
    public Cursor getAllBook() {
        return db.query(BookInfomation, new String[]{columBookID, bookName, auThor, publisherCom},
                null, null, null, null, null);
    }

    /*--------------------updateBook----------------------------*/
    public boolean updateBook(long rowId, String name, String authors, String pcs) {
        ContentValues args = new ContentValues();
        args.put(bookName, name);
        args.put(auThor, authors);
        args.put(publisherCom, pcs);
        return db.update(BookInfomation, args, columBookID + "=" + rowId, null) > 0;
    }

    /*-------------------------delete book----------------------------*/
    public boolean deleteBook(long rowId) {
        return db.delete(BookInfomation, columBookID + "=" + rowId, null) > 0;
    }

    public Cursor searchBook(String inputText) throws SQLException {
        Log.w(TAG, inputText);
        Cursor mCursor = null;
        if (inputText == null || inputText.length() == 0) {
            mCursor = db.query(BookInfomation, new String[]{columBookID,
                            bookName, auThor, publisherCom},
                    null, null, null, null, null);

        } else {
            mCursor = db.query(true, BookInfomation, new String[]{columBookID,
                            bookName, auThor, publisherCom},
                    borrowbookName + " like '%" + inputText + "%'", null,
                    null, null, null, null);
        }
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }

    /*-------------------------Spinner----------------------------*/
    public List<String> getAllBookName() {
        List<String> list = new ArrayList<String>();
        String selectQuery = "SELECT  * FROM " + BookInfomation;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                list.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return list;
    }

    /*-------------------------insertBorrowBook----------------------------*/
    public long insertBorrowBook(String date, String book, String person) {

        ContentValues initialValues = new ContentValues();
        initialValues.put(borrowDate, date);
        initialValues.put(borrowbookName, book);
        initialValues.put(readerName, person);
        open();
        return db.insert(BorrowBookTable, null, initialValues);
    }

    /*----------------------------all borrow book----------------------*/
    public Cursor getAllBorrowBook() {
        return db.query(BorrowBookTable, new String[]{columBorrowID, borrowDate, borrowbookName, readerName},
                null, null, null, null, null);
    }

    /*-------------------------delete borrow----------------------------*/
    public boolean deleteBorrowBook(long rowId) {
        return db.delete(BorrowBookTable, columBorrowID + "=" + rowId, null) > 0;
    }

//
//    public Cursor getchi(String date1, String date2) {
//        return db.query(KhoanChiTable, new String[]{colkcID, mathloaichi, colsotienkc,
//                colngaychi}, colngaychi + " >= " + date1 + " and " + colngaychi + " <= " + date2, null, null, null, null);
//    }
//
//    /*--------------Liệt kê _id Khoản Chi----------------------------*/
//    public Cursor getkhoanchi(long rowId) throws SQLException {
//        SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
//        String colngaychi = sdf.format(new Date());
//        Cursor mCursor = db.query(true, KhoanChiTable, new String[]{
//                        colkcID, mathloaichi, colsotienkc, colngaychi}, colkcID + "=" + rowId, null, null,
//                null, null, null);
//        if (mCursor != null) {
//            mCursor.moveToFirst();
//        }
//        return mCursor;
//    }


    /*--------------------updateBorrowBook----------------------------*/
    public boolean updateBorrowBook(long rowId, String date, String book, String person) {
        ContentValues args = new ContentValues();
        args.put(borrowDate, date);
        args.put(borrowbookName, book);
        args.put(readerName, person);
        return db.update(BorrowBookTable, args, columBorrowID + "=" + rowId, null) > 0;
    }

    /*--------------------------------searchBookName---------------------------------*/
    public Cursor searchBookName(String inputText) throws SQLException {
        Log.w(TAG, inputText);
        Cursor mCursor = null;
        if (inputText == null || inputText.length() == 0) {
            mCursor = db.query(BorrowBookTable, new String[]{columBorrowID,
                            borrowDate, borrowbookName, readerName},
                    null, null, null, null, null);

        } else {
            mCursor = db.query(true, BorrowBookTable, new String[]{columBorrowID,
                            borrowDate, borrowbookName, readerName},
                    borrowbookName + " like '%" + inputText + "%'", null,
                    null, null, null, null);
        }
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }

}
