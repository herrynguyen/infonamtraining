package com.example.herrynguyen.testfragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class BookManage extends Fragment implements View.OnClickListener {
    private View rootView;
    private MainActivity main;
    private Button addBorrow, listBorrow;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        main = (MainActivity) getActivity();
        rootView = inflater.inflate(R.layout.addborrowbook, null);
        addBorrow = (Button) rootView.findViewById(R.id.btnAddBorrow);
        listBorrow = (Button) rootView.findViewById(R.id.btnListBorrow);
        addBorrow.setOnClickListener(this);
        listBorrow.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddBorrow:
                Intent borrowIntent1 = new Intent(getActivity().getBaseContext(), BorrowBook.class);
                borrowIntent1.putExtra("tab", 0);
                getActivity().startActivity(borrowIntent1);
                break;
            case R.id.btnListBorrow:
                Intent borrowIntent2 = new Intent(getActivity().getBaseContext(), BorrowBook.class);
                borrowIntent2.putExtra("tab", 1);
                getActivity().startActivity(borrowIntent2);
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }
}
