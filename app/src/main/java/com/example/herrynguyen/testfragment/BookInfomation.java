package com.example.herrynguyen.testfragment;

import android.app.AlertDialog.Builder;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BookInfomation extends Activity {
    Calendar cal;
    private DataAdapter dataAdapter;
    private Date dateFinish;
    private MainActivity main;
    private Button add;
    private ListView listBook;
    private EditText bookName, author, publisherCompany;
    SimpleCursorAdapter cursorData;
    private CheckInputType checkInputType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_book);
        Intent intent = getIntent();
        int tab = intent.getIntExtra("tab", 0);
        add = (Button) findViewById(R.id.btnAddBookInfo);
        bookName = (EditText) findViewById(R.id.edtName);
        author = (EditText) findViewById(R.id.edtAuthor);
        publisherCompany = (EditText) findViewById(R.id.edtPC);
        listBook = (ListView) findViewById(R.id.listBook);
        checkInputType = new CheckInputType(this);
        //Put all Book to ListView
        dataAdapter = new DataAdapter(this);
        dataAdapter.open();
        Cursor cursor = dataAdapter.getAllBook();
        startManagingCursor(cursor);
        String[] colum = new String[]{DataAdapter.bookName, DataAdapter.auThor, DataAdapter.publisherCom};
        int[] row = new int[]{R.id.name, R.id.author, R.id.pc};
        cursorData = new SimpleCursorAdapter(this, R.layout.row_bookinfo, cursor, colum, row);
        listBook.setAdapter(cursorData);
        //Edit listView
        editBookInfo();
        loadTabs(tab);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = bookName.getText().toString().trim();
                String authors = author.getText().toString().trim();
                String pcs = publisherCompany.getText().toString().trim();
                //Check input data
                if (name.equals("") || authors.equals("") || pcs.equals("")) {
                    checkInputType.alertDialogNullData();
                } else {
                    dataAdapter.open();
                    List<String> listBook = dataAdapter.getAllBookName();
                    int pos = 0;
                    for (int i = 0; i < listBook.size(); i++) {
                        if (name.equals(listBook.get(i))) {
                            pos = 1;
                        }
                    }
                    if (pos == 0) {
                        dataAdapter.insertBook(name, authors, pcs);
                        Toast.makeText(getApplicationContext(), "Success !", Toast.LENGTH_SHORT).show();
                        editBookInfo();
                        bookName.setText(null);
                        author.setText(null);
                        publisherCompany.setText(null);
                    } else {
                        checkInputType.alertDialogExists();
                    }
                }
            }
        });
    }

    //load tabhost

    public void loadTabs(int i) {
        final TabHost tab = (TabHost) findViewById(android.R.id.tabhost);
        tab.setup();
        TabHost.TabSpec spec;

        spec = tab.newTabSpec("tab1");
        spec.setContent(R.id.tab1BorrowBook);
        spec.setIndicator("Book Infomation");
        tab.addTab(spec);

        spec = tab.newTabSpec("t2");
        spec.setContent(R.id.tab2BorrowBook);
        spec.setIndicator("List Book");
        tab.addTab(spec);

        tab.setCurrentTab(i);
    }

    private void editBookInfo() {
        try {
            dataAdapter.open();
            Cursor cursor = dataAdapter.getAllBook();
            startManagingCursor(cursor);
            String[] colum = new String[]{DataAdapter.bookName, DataAdapter.auThor, DataAdapter.publisherCom};
            int[] row = new int[]{R.id.name, R.id.author, R.id.pc};
            cursorData = new SimpleCursorAdapter(this, R.layout.row_bookinfo, cursor, colum, row);
            listBook.setAdapter(cursorData);

            listBook.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @SuppressLint("NewApi")
                @Override
                public void onItemClick(AdapterView<?> parent, View arg1, int position, long id) {
                    // TODO Auto-generated method stub
                    //
                    final Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                    final int item_id = cursor.getInt(cursor.getColumnIndex(DataAdapter.columBookID));
                    String item_name = cursor.getString(cursor.getColumnIndex(DataAdapter.bookName));
                    String item_author = cursor.getString(cursor.getColumnIndex(DataAdapter.auThor));
                    String item_pc = cursor.getString(cursor.getColumnIndex(DataAdapter.publisherCom));

                    AlertDialog.Builder myDialog = new AlertDialog.Builder(BookInfomation.this);

                    myDialog.setTitle("Delete/Edit?");
                    final EditText nameofbook = new EditText(BookInfomation.this);
                    final EditText nameofauthor = new EditText(BookInfomation.this);
                    final EditText nameofpc = new EditText(BookInfomation.this);

                    ActionBar.LayoutParams book_layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    ActionBar.LayoutParams author_layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    ActionBar.LayoutParams pc_layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                    nameofbook.setLayoutParams(book_layout);
                    nameofauthor.setLayoutParams(author_layout);
                    nameofpc.setLayoutParams(pc_layout);

                    nameofbook.setText(item_name);
                    nameofauthor.setText(item_author);
                    nameofpc.setText(item_pc);

                    LinearLayout layout = new LinearLayout(BookInfomation.this);

                    layout.setOrientation(LinearLayout.VERTICAL);
                    layout.addView(nameofbook);
                    layout.addView(nameofauthor);
                    layout.addView(nameofpc);

                    myDialog.setView(layout);

                    myDialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {

                            LinearLayout layout = new LinearLayout(BookInfomation.this);
                            AlertDialog.Builder builder = new AlertDialog.Builder(layout.getContext());
                            builder.setTitle("Confirm!");
                            builder.setMessage("Delete this book?");
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated
                                    dataAdapter.open();
                                    dataAdapter.deleteBook(item_id);
                                    Cursor c = dataAdapter.getAllBook();
                                    startManagingCursor(c);
                                    String[] colum = new String[]{DataAdapter.bookName, DataAdapter.auThor, DataAdapter.publisherCom};
                                    for (int i = 0; i < colum.length; i++) {
                                        Log.e("BookName", colum[i]);
                                    }
                                    int[] row = new int[]{R.id.name, R.id.author, R.id.pc};
                                    SimpleCursorAdapter simpleCursor = new SimpleCursorAdapter(BookInfomation.this, R.layout.row_bookinfo, c, colum, row);
                                    listBook.setAdapter(simpleCursor);
                                    simpleCursor.notifyDataSetChanged();
                                    dataAdapter.close();
                                }
                            });
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated
                                    dialog.dismiss();
                                }
                            });
                            builder.create().show();
                        }
                    });

                    myDialog.setNeutralButton("Update", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            String valueName = nameofbook.getText().toString().trim();
                            String valueAuthor = nameofauthor.getText().toString().trim();
                            String valuePC = nameofpc.getText().toString().trim();
                            //Check input data
                            if (valueName.equals("") || valueAuthor.equals("") || valuePC.equals("")) {
                                checkInputType.alertDialogNullData();
                            } else {
                                dataAdapter.open();
                                List<String> listB = dataAdapter.getAllBookName();
                                int pos = 0;
                                for (int i = 0; i < listB.size(); i++) {
                                    if (valueName.equals(listB.get(i))) {
                                        pos = 1;
                                    }
                                }
                                if (pos == 0) {
                                    dataAdapter.updateBook(item_id, valueName, valueAuthor, valuePC);
                                    Cursor cursor = dataAdapter.getAllBook();
                                    startManagingCursor(cursor);

                                    String[] colum = new String[]{DataAdapter.bookName, DataAdapter.auThor, DataAdapter.publisherCom};
                                    int[] row = new int[]{R.id.name, R.id.author, R.id.pc};

                                    SimpleCursorAdapter notes = new SimpleCursorAdapter(BookInfomation.this, R.layout.row_bookinfo, cursor, colum, row);
                                    listBook.setAdapter(notes);
                                    Toast.makeText(getApplicationContext(), "Edited: " + valueName, Toast.LENGTH_LONG).show();
                                    notes.notifyDataSetChanged();
                                    dataAdapter.close();
                                } else {
                                    checkInputType.alertDialogExists();
                                }
                            }
                        }
                    });
                    myDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    });
                    myDialog.show();
                }
            });

        } catch (Exception e) {
            System.out.println(e);
        }
        dataAdapter.close();
    }
}
