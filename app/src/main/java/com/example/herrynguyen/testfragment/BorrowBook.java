package com.example.herrynguyen.testfragment;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BorrowBook extends Activity {
    Calendar cal;
    private DataAdapter dataAdapter;
    private Date dateFinish;
    private MainActivity main;
    private ListView listBorrow;
    private Button getDate, add;
    Spinner spinner;
    private EditText dateBorrow, bookName, reader;
    SimpleCursorAdapter cursorData;
    private CheckInputType checkInputType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borrow_book);
        Intent intent = getIntent();
        int tab = intent.getIntExtra("tab", 0);
        getDate = (Button) findViewById(R.id.getDate);
        add = (Button) findViewById(R.id.btnAddBorrowInfo);
        dateBorrow = (EditText) findViewById(R.id.edtDate);
        spinner = (Spinner) findViewById(R.id.spinnerBookName);
        reader = (EditText) findViewById(R.id.edtReader);
        listBorrow = (ListView) findViewById(R.id.listBorrowBook);
        checkInputType = new CheckInputType(this);

        dataAdapter = new DataAdapter(main);
        dataAdapter.open();

        Cursor cursor = dataAdapter.getAllBorrowBook();
        String[] colum = new String[]{DataAdapter.borrowDate, DataAdapter.borrowbookName, DataAdapter.readerName};
        int[] row = new int[]{R.id.date, R.id.bookName, R.id.reader};
        cursorData = new SimpleCursorAdapter(this, R.layout.row_borrowinfo, cursor, colum, row);
        listBorrow.setAdapter(cursorData);

        editBorrowInfo();
        loadSpinnerData();
        loadTabs(tab);

        //getCurentDay
        getDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                getDefaultDateTime();
                showDatePicker();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String date = dateBorrow.getText().toString().trim();
                String book = (String) spinner.getSelectedItem().toString().trim();
                String read = reader.getText().toString().trim();
                //Check input data
                if (date.equals("") || book.equals("") || read.equals("")) {
                    checkInputType.alertDialogNullData();
                } else if (checkInputType.isValidate(date)) {
                    dataAdapter.insertBorrowBook(date, book, read);
                    Toast.makeText(getApplicationContext(), "Success !", Toast.LENGTH_SHORT).show();
                    editBorrowInfo();
                    reader.setText(null);
                } else {
                    checkInputType.alertDateFormat();
                }
            }
        });
    }

    public void getDefaultDateTime() {
        cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String strDate = dateFormat.format(cal.getTime());
        dateBorrow.setText(strDate);
        dateFinish = cal.getTime();
    }

    public void showDatePicker() {
        DatePickerDialog.OnDateSetListener callback = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dateBorrow.setText((dayOfMonth) + "/" + (monthOfYear + 1) + "/" + year);
                cal.set(year, monthOfYear, dayOfMonth);
                dateFinish = cal.getTime();
            }
        };
        String getDate = dateBorrow.getText() + "";
        String strArrtmp1[] = getDate.split("/");
        int date = Integer.parseInt(strArrtmp1[0]);
        int month = Integer.parseInt(strArrtmp1[1]) - 1;
        int year = Integer.parseInt(strArrtmp1[2]);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, callback, year, month, date);
        datePickerDialog.setTitle("Select Date");
        datePickerDialog.show();
    }

    public void loadTabs(int i) {
        final TabHost tab = (TabHost) findViewById(android.R.id.tabhost);
        tab.setup();
        TabHost.TabSpec spec;

        spec = tab.newTabSpec("tab1");
        spec.setContent(R.id.tab1BorrowBook);
        spec.setIndicator("Borrow Infomation");
        tab.addTab(spec);

        spec = tab.newTabSpec("t2");
        spec.setContent(R.id.tab2BorrowBook);
        spec.setIndicator("List Borrow");
        tab.addTab(spec);

        tab.setCurrentTab(i);
    }

    private void loadSpinnerData() {
        dataAdapter.open();
        List<String> lables = dataAdapter.getAllBookName();
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lables);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
    }

    private void editBorrowInfo() {
        try {
            dataAdapter.open();
            Cursor cursor = dataAdapter.getAllBorrowBook();
            startManagingCursor(cursor);
            String[] colum = new String[]{DataAdapter.borrowDate, DataAdapter.borrowbookName, DataAdapter.readerName};
            int[] row = new int[]{R.id.date, R.id.bookName, R.id.reader};
            cursorData = new SimpleCursorAdapter(this, R.layout.row_borrowinfo, cursor, colum, row);
            listBorrow.setAdapter(cursorData);

            listBorrow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @SuppressLint("NewApi")
                @Override
                public void onItemClick(AdapterView<?> parent, View arg1, int position, long id) {
                    // TODO Auto-generated method stub

                    final Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                    final int item_id = cursor.getInt(cursor.getColumnIndex(DataAdapter.columBorrowID));
                    String item_date = cursor.getString(cursor.getColumnIndex(DataAdapter.borrowDate));
                    String item_name = cursor.getString(cursor.getColumnIndex(DataAdapter.borrowbookName));
                    String item_reader = cursor.getString(cursor.getColumnIndex(DataAdapter.readerName));

                    AlertDialog.Builder myDialog = new AlertDialog.Builder(BorrowBook.this);

                    myDialog.setTitle("Delete/Edit?");
                    final EditText borrowDate = new EditText(BorrowBook.this);
                    //final EditText nameofBook = new EditText(BorrowBook.this);
                    final Spinner nameofBook = new Spinner(BorrowBook.this);
                    final EditText nameofReader = new EditText(BorrowBook.this);


                    ActionBar.LayoutParams date_layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    ActionBar.LayoutParams name_layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    ActionBar.LayoutParams reader_layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);


                    borrowDate.setLayoutParams(date_layout);
                    nameofBook.setLayoutParams(name_layout);
                    nameofReader.setLayoutParams(reader_layout);

                    dataAdapter.open();
                    List<String> listBook = dataAdapter.getAllBookName();
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(BorrowBook.this, android.R.layout.simple_spinner_item, listBook);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    nameofBook.setAdapter(arrayAdapter);

                    //Set posision to spinner
                    int pos = -1;
                    for (int i = 0; i < listBook.size(); i++) {
                        if (item_name.equals(listBook.get(i))) {
                            pos = i;
                        }
                    }
                    if (pos >= 0) {
                        nameofBook.setSelection(pos);
                    }
                    borrowDate.setText(item_date);
                    nameofReader.setText(item_reader);

                    LinearLayout layout = new LinearLayout(BorrowBook.this);

                    layout.setOrientation(LinearLayout.VERTICAL);
                    layout.addView(borrowDate);
                    layout.addView(nameofBook);
                    layout.addView(nameofReader);

                    myDialog.setView(layout);

                    myDialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            LinearLayout layout = new LinearLayout(BorrowBook.this);
                            AlertDialog.Builder builder = new AlertDialog.Builder(layout.getContext());
                            builder.setTitle("Confirm!");
                            builder.setMessage("Delete this infomation?");
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated
                                    dataAdapter.open();
                                    dataAdapter.deleteBorrowBook(item_id);

                                    Cursor cursor = dataAdapter.getAllBorrowBook();
                                    startManagingCursor(cursor);
                                    String[] colum = new String[]{DataAdapter.borrowDate, DataAdapter.borrowbookName, DataAdapter.readerName};
                                    int[] row = new int[]{R.id.date, R.id.bookName, R.id.reader};
                                    SimpleCursorAdapter simpleCursor = new SimpleCursorAdapter(BorrowBook.this, R.layout.row_borrowinfo, cursor, colum, row);
                                    listBorrow.setAdapter(simpleCursor);
                                    simpleCursor.notifyDataSetChanged();
                                    dataAdapter.close();
                                }
                            });
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated
                                    dialog.dismiss();
                                }
                            });
                            builder.create().show();
                        }
                    });

                    myDialog.setNeutralButton("Update", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            String valueDate = borrowDate.getText().toString().trim();
                            String valueName = (String) nameofBook.getSelectedItem().toString().trim();
                            String valueReader = nameofReader.getText().toString().trim();
                            int count=0;
                            if (valueDate.equals("") || valueName.equals("") || valueReader.equals("")) {
                                checkInputType.alertDialogNullData();
                                count++;
                            } else if (!checkInputType.isValidate(valueDate)) {
                                checkInputType.alertDateFormat();
                                count++;
                            }
                            else/* if (count==0)*/{
                                dataAdapter.open();
                                dataAdapter.updateBorrowBook(item_id, valueDate, valueName, valueReader);
                                Cursor cursor = dataAdapter.getAllBorrowBook();
                                startManagingCursor(cursor);
                                String[] colum = new String[]{DataAdapter.borrowDate, DataAdapter.borrowbookName, DataAdapter.readerName};
                                int[] row = new int[]{R.id.date, R.id.bookName, R.id.reader};
                                SimpleCursorAdapter notes = new SimpleCursorAdapter(BorrowBook.this, R.layout.row_borrowinfo, cursor, colum, row);
                                listBorrow.setAdapter(notes);

                                Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_SHORT).show();
                                notes.notifyDataSetChanged();
                                dataAdapter.close();
                            }
                        }
                    });
                    myDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    });
                    myDialog.show();
                }
            });

        } catch (Exception e) {
            System.out.println(e);
        }
        dataAdapter.close();
    }
}
