package com.example.herrynguyen.testfragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import java.util.Date;
import java.text.ParseException;

import java.text.SimpleDateFormat;

/**
 * Created by herry.nguyen on 1/5/2016.
 */
public class CheckInputType {
    private final Context context;
    public CheckInputType(Context ctx) {
        this.context = ctx;
    }
    //alertdialogNullData
    public void alertDialogNullData(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Notification!")
                .setCancelable(false)
                .setMessage("You must enter all information!")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.create().show();
    }
    //alertdialogExists
    public void alertDialogExists(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Notification!")
                .setCancelable(false)
                .setMessage("This book aready exists!")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.create().show();
    }
    //Method check input dateTime format
    public boolean isValidate(String dateToValidate) {
        String dateFormat = "dd/MM/yyyy";
        if (dateToValidate.equals("")) {
            return false;
        }
        SimpleDateFormat simpleFormat = new SimpleDateFormat(dateFormat);
        simpleFormat.setLenient(false);
        try {
            Date date = simpleFormat.parse(dateToValidate);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }
    //alertdialogExists
    public void alertDateFormat(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Notification!")
                .setCancelable(false)
                .setMessage("Wrong datetime format!")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.create().show();
    }
}
